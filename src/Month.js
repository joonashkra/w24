import React, { useState } from 'react'
import Day from './Day';

function Month() {

    const months = [];
    for(let i=1; i<=12; i++) {
      months.push(i)
    }

    const [selectedMonth, setSelectedMonth] = useState("")

    const handleSelectChange = (event) => {
        setSelectedMonth(event.target.value);
    }

  return (
    <div>
        <select onChange={handleSelectChange} value={selectedMonth}>
                {months.map((month) => (
                    <option key={month} value={month}>
                        {month}
                    </option>
                ))}
        </select>
        <Day month={selectedMonth}/>
    </div>
  )
}

export default Month