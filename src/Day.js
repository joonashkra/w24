import React from 'react'

function Day(props) {

const month = props.month

const getFirstDay = (month) => {
  const firstDay = new Date(2021, month - 1, 1)
  return firstDay.getDay()
}

const days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
const firstDay = getFirstDay(month)

return (
    <div>{`Month ${month}/2021 starts ${days[firstDay]}`}</div>
);
}

export default Day